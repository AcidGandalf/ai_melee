﻿// The MIT License(MIT)
//
// Copyright 2019 Vii
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files(the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions :
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

#include <time.h>
#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <string>
#include <locale>
#include <codecvt>
#include <fstream>
#include <iostream>
#include <deque>
#include <unordered_map>
#include <random>
#include <utility>
#include <vector>
#include "engine/easy.h"
#include "engine/font.h"

#include <iostream>

using namespace arctic;  // NOLINT

#pragma execution_character_set("utf-8")

enum CellKind {
  kCellFloor = 0,
  kCellObstacleWall,
  kCellObstacleSnowman,
  kCellObstacleBag
};

enum VisKind {
  kVisFloor = 0,
  kVisObstacle,
  kVisMyHead,
  kVisOtherHead,
  kVisMyBody,
  kVisOtherBody,
  kVisMyTail,
  kVisOtherTail,
  kVisNone
};

struct Vis {
  VisKind kind = kVisFloor;
};

struct Code {
  VisKind kind = kVisNone;
  bool is_exclude = false;
  bool is_and = false;
};

Vec2Si32 g_vis_sz(7, 7);

struct Card {
  Code code[7][7];
};

enum CommandKind {
  kCommandNone = 0,
  kCommandSelectTool,
  kCommandEditCard
};

struct Command {
  CommandKind kind = kCommandNone;
  Code from_code;
  Code to_code;
  Si32 snake_idx = 0;
  Si32 card_idx = 0;
  Vec2Si32 card_pos = Vec2Si32(0, 0);
};

struct EatMark {
  Vec2Si32 pos;
  double time;
  EatMark(Vec2Si32 position, double creation_time) {
    pos = position;
    time = creation_time;
  }
};

enum Control {
  kControlBegin = 0,
  kControlRestart = kControlBegin,
  kControlStep,
  kControlPlay,
  kControlPlayFast,
  kControlPlay100,
  kControlEnd
};

enum ButtonState {
  kButtonBegin = 0,
  kButtonDown = kButtonBegin,
  kButtonHover,
  kButtonUp,
  kButtonEnd
};

struct Button {
  Control control = kControlBegin;
  ButtonState state = kButtonUp;
  Vec2Si32 pos;
  std::vector<Sprite> sprites;
  
  void Update() {
    if (state == kButtonDown || sprites.size() == 0) {
      return;
    }
    Vec2Si32 mouse = MousePos() - pos;
    if (mouse.x >= 0 && mouse.y >= 0
        && mouse.x < sprites[0].Width() && mouse.y < sprites[0].Height()) {
      state = kButtonHover;
      if (IsKeyDownward(kKeyMouseLeft)) {
        state = kButtonDown;
      }
    } else {
      state = kButtonUp;
    }
  }
  
  Button(Vec2Si32 button_pos, Control button_control,
         std::vector<Sprite> &button_sprites) {
    pos = button_pos;
    control = button_control;
    sprites = button_sprites;
  }
  
  void Draw() {
    if (sprites.size() > state) {
      sprites[state].Draw(pos);
    }
  }
};

std::vector<::Button> g_buttons;
Control g_control;

std::deque<EatMark> g_eat_marks;

struct Snake {
  std::deque<Vec2Si32> body; // begin - head, end - tail
  Vis vis[7][7]; // y, x
  Card card[12];
  // 0 1 2
  // 3 4 5
  // 6 7 8
  
  void SetVis(Vec2Si32 pos, VisKind kind) {
    if (body.size()) {
      Vec2Si32 corner = body.front() - g_vis_sz / 2;
      if (pos.x >= corner.x && pos.y >= corner.y &&
          pos.x < corner.x + g_vis_sz.x && pos.y < corner.y + g_vis_sz.y) {
        vis[pos.y - corner.y][pos.x - corner.x].kind = kind;
      }
    }
  }
};

struct Cell {
  CellKind kind = kCellObstacleWall;
};

enum BattleResult {
  BattleInProgress,
  BattleWon,
  BattleLost,
  BattleDraw,
  BattleLevelComplete
};

BattleResult g_battle_result;


Sprite g_sprite_sheet;

Sprite g_empty;

Sprite g_cross;
Sprite g_and_floor;
Sprite g_none_floor;
Sprite g_own_body;
Sprite g_enemy_body;
Sprite g_obstacle;

std::vector<Sprite> g_tail_up;
std::vector<Sprite> g_tail_right;
std::vector<Sprite> g_tail_down;
std::vector<Sprite> g_tail_left;

std::vector<Sprite> g_body_du;
std::vector<Sprite> g_body_lr;
std::vector<Sprite> g_body_ru;
std::vector<Sprite> g_body_dr;
std::vector<Sprite> g_body_dl;
std::vector<Sprite> g_body_lu;

std::vector<Sprite> g_head_up;
std::vector<Sprite> g_head_right;
std::vector<Sprite> g_head_down;
std::vector<Sprite> g_head_left;

Sprite g_floor;
Sprite g_obstacle_wall;
Sprite g_bite;
Sprite g_obstacle_snowman;
Sprite g_obstacle_bag;
Sprite g_empty_floor;

Sprite g_controls;

Sprite g_glory;
Sprite g_wasted;
Sprite g_draw;
Sprite g_complete;

Sprite g_font;
std::unordered_map<char32_t, Sprite> g_ch;

Sprite g_help;

Vec2Si32 g_sz(20, 20);
Vec2Si32 g_control_sz(40, 40);
Vec2Si32 g_map_size(27, 27);
Cell g_map[27][27]; // y, x
const Si32 kStartLength = 10;

std::vector<Snake> g_snakes;
Si64 g_battle_step;

std::string g_tool_info;
Code g_tool;
Vec2Si32 g_tools_offset;

std::string g_100_stats;

double g_prev_time = 0.0;
double g_prev_step_time = 0.0;

Si32 g_level_idx = 1;

Command EditCard(Si32 snake_idx, Si32 card_idx, Vec2Si32 card_pos) {
  Command cmd;
  cmd.kind = kCommandEditCard;
  cmd.snake_idx = snake_idx;
  cmd.card_idx = card_idx;
  cmd.card_pos = card_pos;
  cmd.from_code =
    g_snakes[snake_idx].card[card_idx].code[card_pos.y][card_pos.x];
  if (g_tool.kind == cmd.from_code.kind
      && g_tool.is_and == cmd.from_code.is_and
      && g_tool.is_exclude == cmd.from_code.is_exclude) {
    cmd.to_code.kind = kVisNone;
    cmd.to_code.is_exclude = false;
    cmd.to_code.is_and = false;
  } else {
    cmd.to_code = g_tool;
  }
  return cmd;
}

Command SelectTool(Code tool) {
  Command cmd;
  cmd.kind = kCommandSelectTool;
  cmd.from_code = g_tool;
  cmd.to_code = tool;
  return cmd;
}

Sprite& CellSprite(CellKind kind) {
  switch (kind) {
    case kCellFloor:
      return g_floor;
    case kCellObstacleWall:
      return g_obstacle_wall;
    case kCellObstacleSnowman:
      return g_obstacle_snowman;
    case kCellObstacleBag:
      return g_obstacle_bag;
    default:
      Fatal("Unknown cell kind in CellSprite");
      return g_empty;
  }
}


Cell& Map(Vec2Si32 pos) {
  if (pos.x >= 0 && pos.y >= 0
      && pos.x < g_map_size.x && pos.y < g_map_size.y) {
    return g_map[pos.y][pos.x];
  } else {
    return g_map[0][0];
  }
}

std::independent_bits_engine<std::mt19937_64, 8, Ui64> g_rnd;

Sprite InitSprite(Si32 x, Si32 y) {
  Sprite s;
  s.Reference(g_sprite_sheet, x * g_sz.x, y * g_sz.y, g_sz.x, g_sz.y);
  return s;
}

void ResetBattle() {
  g_battle_step = 0;
  g_battle_result = BattleInProgress;
  g_snakes[0].body.clear();
  g_snakes[1].body.clear();
  for (Si32 i = 0; i < kStartLength; ++i) {
    g_snakes[0].body.push_front(
                                //Vec2Si32(1 + i, g_map_size.y / 2 - 1));
                                Vec2Si32(1 + i, g_map_size.y / 2));
    g_snakes[1].body.push_front(
                                //Vec2Si32(g_map_size.x - 2 - i, g_map_size.y / 2 + 1));
                                Vec2Si32(g_map_size.x - 2 - i, g_map_size.y / 2 ));
  }
  g_eat_marks.clear();
}

void DrawText(Si32 x, Si32 y, const char *text) {
  if (text == nullptr) {
    return;
  }
//  std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> cvt;
//  std::u32string utf32 = cvt.from_bytes(text);
//  for (auto chit = utf32.begin(); chit != utf32.end(); ++chit) {

  for (const Ui8* p = (const Ui8*)text; *p != 0;) {
      Ui32 u = 0;
      if ((p[0] & 0x80) == 0) {
          u = p[0];
          p++;
      } else if ((p[0] & 0xe0) == 0xc0) {
          u = (Ui32(p[0]) << 8) | (Ui32(p[1]));
          p += 2;
      } else if ((p[0] & 0xf0) == 0xe0) {
          u = (Ui32(p[0]) << 16) | (Ui32(p[1]) << 8) | (Ui32(p[2]));
          p += 3;
      } else if ((p[0] & 0xf8) == 0xf0) {
          u = (Ui32(p[0]) << 24) | (Ui32(p[1]) << 16) | (Ui32(p[2]) << 8) | (Ui32(p[3]));
          p += 4;
      } else {
          p++;
      }
    auto it = g_ch.find(u);
    if (it != g_ch.end()) {
      it->second.Draw(x, y);
    }
    x += 8;
    ++text;
  }
}

void Init() {
  g_rnd.seed(clock());
  ResizeScreen(1024, 720);
  g_sprite_sheet.Load("data/snakes.tga");
  g_controls.Load("data/controls.tga");
  g_glory.Load("data/glory.tga");
  g_wasted.Load("data/wasted.tga");
  g_draw.Load("data/draw.tga");
  g_complete.Load("data/complete.tga");
  g_font.Load("data/font.tga");
  g_help.Load("data/help.tga");
  
  {
    std::vector<std::string> vec = {
u8"П", u8"Я", u8"Р", u8"С", u8"Т", u8"У", u8"Ж", u8"В", u8"Ь", u8"Ы", u8"З", u8"Ш", u8"Э", u8"Щ", u8"Ч", u8"Ъ",
u8"Ю", u8"А", u8"Б", u8"Ц", u8"Д", u8"Е", u8"Ф", u8"Г", u8"Х", u8"И", u8"Й", u8"К", u8"Л", u8"М", u8"Н", u8"О",
u8"п", u8"я", u8"р", u8"с", u8"т", u8"у", u8"ж", u8"в", u8"ь", u8"ы", u8"з", u8"ш", u8"э", u8"щ", u8"ч", u8"ъ",
u8"ю", u8"а", u8"б", u8"ц", u8"д", u8"е", u8"ф", u8"г", u8"х", u8"и", u8"й", u8"к", u8"л", u8"м", u8"н", u8"о",
u8"p", u8"q", u8"r", u8"s", u8"t", u8"u", u8"v", u8"w", u8"x", u8"y", u8"z", u8"{", u8"|", u8"}", u8" ", u8" ",
u8"`", u8"a", u8"b", u8"c", u8"d", u8"e", u8"f", u8"g", u8"h", u8"i", u8"j", u8"k", u8"l", u8"m", u8"n", u8"o",
u8"P", u8"Q", u8"R", u8"S", u8"T", u8"U", u8"V", u8"W", u8"X", u8"Y", u8"Z", u8"[", u8"\\", u8"]", u8" ", u8"_",
u8"@", u8"A", u8"B", u8"C", u8"D", u8"E", u8"F", u8"G", u8"H", u8"I", u8"J", u8"K", u8"L", u8"M", u8"N", u8"O",
u8"0", u8"1", u8"2", u8"3", u8"4", u8"5", u8"6", u8"7", u8"8", u8"9", u8":", u8";", u8"<", u8"=", u8">", u8"?",
u8" ", u8"!", u8"\"", u8"#", u8"$", u8"%", u8"&", u8"'", u8"(", u8")", u8"*", u8"+", u8",", u8"-", u8".", u8"/"};
    
    for (Si32 idx = 0; idx < vec.size(); ++idx) {
      Si32 x = idx % 16;
      Si32 y = idx / 16;
      //std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> cvt;
      //std::u32string utf32 = cvt.from_bytes(vec[idx]);
      const Ui8* p = (const Ui8*)vec[idx].data();
      Ui32 u = 0;
      if ((p[0] & 0x80) == 0) {
        u = p[0];
        p++;
       } else if ((p[0] & 0xe0) == 0xc0) {
        u = (Ui32(p[0]) << 8) | (Ui32(p[1]));
        p += 2;
      } else if ((p[0] & 0xf0) == 0xe0) {
          u = (Ui32(p[0]) << 16) | (Ui32(p[1]) << 8) | (Ui32(p[2]));
          p += 3;
      } else if ((p[0] & 0xf8) == 0xf0) {
          u = (Ui32(p[0]) << 24) | (Ui32(p[1]) << 16) | (Ui32(p[2]) << 8) | (Ui32(p[3]));
          p += 4;
      } else {
          p++;
      }
      g_ch[u].Reference(g_font, x * 8, y * 11, 8, 11);
    }
  }
  
  
  g_cross = InitSprite(0, 0);
  g_and_floor = InitSprite(1, 0);
  g_none_floor = InitSprite(2, 0);
  g_own_body = InitSprite(3, 0);
  g_enemy_body = InitSprite(4, 0);
  g_obstacle = InitSprite(5, 0);
  
  for (Si32 snake_idx = 0; snake_idx < 2; ++snake_idx) {
    Si32 base = (snake_idx == 0 ? 1 : 4);
    g_tail_up.push_back(InitSprite(0, base));
    g_tail_right.push_back(InitSprite(1, base));
    g_tail_down.push_back(InitSprite(2, base));
    g_tail_left.push_back(InitSprite(3, base));
    
    g_body_du.push_back(InitSprite(0, base + 1));
    g_body_lr.push_back(InitSprite(1, base + 1));
    g_body_ru.push_back(InitSprite(2, base + 1));
    g_body_dr.push_back(InitSprite(3, base + 1));
    g_body_dl.push_back(InitSprite(4, base + 1));
    g_body_lu.push_back(InitSprite(5, base + 1));
    
    g_head_up.push_back(InitSprite(0, base + 2));
    g_head_right.push_back(InitSprite(1, base + 2));
    g_head_down.push_back(InitSprite(2, base + 2));
    g_head_left.push_back(InitSprite(3, base + 2));
  }
  
  g_floor = InitSprite(0, 7);
  g_obstacle_wall = InitSprite(1, 7);
  g_bite = InitSprite(2, 7);
  
  Sprite snowman = InitSprite(3, 7);
  g_obstacle_snowman.Create(snowman.Width(), snowman.Height());
  g_floor.Draw(0, 0, g_obstacle_snowman.Width(), g_obstacle_snowman.Height(),
               0, 0, g_floor.Width(), g_floor.Height(), g_obstacle_snowman);
  snowman.Draw(0, 0, g_obstacle_snowman.Width(), g_obstacle_snowman.Height(),
               0, 0, g_floor.Width(), g_floor.Height(), g_obstacle_snowman);
  
  Sprite bag = InitSprite(4, 7);
  g_obstacle_bag.Create(bag.Width(), bag.Height());
  g_floor.Draw(0, 0, g_obstacle_bag.Width(), g_obstacle_bag.Height(),
               0, 0, g_floor.Width(), g_floor.Height(), g_obstacle_bag);
  bag.Draw(0, 0, g_obstacle_bag.Width(), g_obstacle_bag.Height(),
           0, 0, g_floor.Width(), g_floor.Height(), g_obstacle_bag);
  
  g_empty_floor = InitSprite(5, 7);
  
  
  Vec2Si32 pos;
  for (pos.y = 1; pos.y < g_map_size.y - 1; ++pos.y) {
    for (pos.x = 1; pos.x < g_map_size.x - 1; ++pos.x) {
      g_map[pos.y][pos.x].kind = kCellFloor;
    }
  }
  g_map[5][5].kind = kCellObstacleSnowman;
  g_map[g_map_size.x - 6][g_map_size.y - 6].kind = kCellObstacleBag;
  
  g_snakes.resize(2);
  
  
  
  for (Si32 snake_idx = 0; snake_idx < g_snakes.size(); ++snake_idx) {
    Snake &s = g_snakes[snake_idx];
    for (Si32 card_idx = 0; card_idx < 12; ++card_idx) {
      Card &c = s.card[card_idx];
      Code &code = c.code[g_vis_sz.y / 2][g_vis_sz.x / 2];
      code.is_exclude = false;
      code.is_and = true;
      code.kind = kVisMyHead;
    }
  }
  
  g_tool.kind = kVisOtherTail;
  
  g_tools_offset = Vec2Si32(
                        g_sz.x * (g_map_size.x + 1),
                        g_sz.y * (4 * (g_vis_sz.y + 1)));
  
  
  for (Si32 control = kControlBegin; control < kControlEnd; ++control) {
    Vec2Si32 pos(g_sz.x * (g_vis_sz.x + 1), g_sz.y * (g_map_size.y + 1));
    std::vector<Sprite> sprites;
    sprites.resize(kButtonEnd);
    for (Si32 idx = kButtonBegin; idx < kButtonEnd; ++idx) {
      sprites[idx].Reference(g_controls,
        control * 40, idx * 40, 40, 40);
    }
    ::Button button(pos + control * Vec2Si32(40, 0), static_cast<Control>(control), sprites);
    g_buttons.push_back(button);
  }
  g_control = kControlEnd;
  
  ResetBattle();
}

void RotateLook(Si32 my_snake_idx) {
  Vis vis[7][7];
  Snake &me = g_snakes[my_snake_idx];
  Vec2Si32 pos;
  for (pos.y = 0; pos.y < g_vis_sz.y; ++pos.y) {
    for (pos.x = 0; pos.x < g_vis_sz.x; ++pos.x) {
      vis[pos.y][pos.x] = me.vis[pos.y][pos.x];
    }
  }
  for (pos.y = 0; pos.y < g_vis_sz.y; ++pos.y) {
    for (pos.x = 0; pos.x < g_vis_sz.x; ++pos.x) {
      me.vis[7 - 1 - pos.x][pos.y] = vis[pos.y][pos.x];
    }
  }
}

void Look(Si32 my_snake_idx) {
  Snake &me = g_snakes[my_snake_idx];
  Vec2Si32 pos;
  for (pos.y = 0; pos.y < g_vis_sz.y; ++pos.y) {
    for (pos.x = 0; pos.x < g_vis_sz.x; ++pos.x) {
      me.vis[pos.y][pos.x].kind = kVisFloor;
    }
  }
  if (me.body.size()) {
    Vec2Si32 base_pos = me.body.front();
    for (pos.y = 0; pos.y < g_vis_sz.y; ++pos.y) {
      for (pos.x = 0; pos.x < g_vis_sz.x; ++pos.x) {
        Vec2Si32 map_pos = base_pos + pos - g_vis_sz / 2;
        Cell &cell = Map(map_pos);
        if (cell.kind != kCellFloor) {
          me.SetVis(map_pos, kVisObstacle);
        }
      }
    }
    
    for (Si32 snake_idx = 0; snake_idx < g_snakes.size(); ++snake_idx) {
      Snake &s = g_snakes[snake_idx];
      bool is_me = (snake_idx == my_snake_idx);
      auto part = s.body.begin();
      if (part != s.body.end()) {
        auto prev_part = part;
        part++;
        if (part != s.body.end()) {
          me.SetVis(*prev_part, (is_me ? kVisMyHead : kVisOtherHead));
        }
        while (part != s.body.end()) {
          if (part == s.body.end()) {
            break;
          }
          auto next_part = part + 1;
          if (next_part == s.body.end()) {
            break;
          }
          me.SetVis(*part, (is_me ? kVisMyBody : kVisOtherBody));
          prev_part = part;
          part++;
        }
        if (part != s.body.end()) {
          me.SetVis(*part, (is_me ? kVisMyTail : kVisOtherTail));
        }
      }
    }
    
  }
}

bool TryMatch(Si32 snake_idx, Si32 card_idx) {
  bool has_code = false;
  bool has_ors = false;
  bool is_or_match = false;
  const Snake &s = g_snakes[snake_idx];
  const Card &card = s.card[card_idx];
  Vec2Si32 pos;
  for (pos.y = 0; pos.y < g_vis_sz.y; ++pos.y) {
    for (pos.x = 0; pos.x < g_vis_sz.x; ++pos.x) {
      const VisKind kind = s.vis[pos.y][pos.x].kind;
      const Code &code = card.code[pos.y][pos.x];
      if (code.kind != kVisNone && code.kind != kVisMyHead) {
        has_code = true;
        if (!code.is_and) {
          has_ors = true;
        }
        if (code.kind == kind) {
          if (code.is_exclude) {
            // exclude match == error
            if (code.is_and) {
              return false;
            }
          } else {
            // match == ok
            if (!code.is_and) {
              is_or_match = true;
            }
          }
        } else {
          // kind does not match
          if (code.is_exclude) {
            // exclude mismatch == ok
            if (!code.is_and) {
              is_or_match = true;
            }
          } else {
            // mismatch == error
            if (code.is_and) {
              return false;
            }
          }
        }
      }
    }
  }
  if (!has_code) {
    return false;
  }
  return is_or_match || !has_ors;
}

bool IsForwardOk(Si32 snake_idx) {
  Vec2Si32 head(3, 3);
  Vec2Si32 new_pos = head + Vec2Si32(0, 1);
  VisKind vis = g_snakes[snake_idx].vis[new_pos.y][new_pos.x].kind;
  if (vis == kVisFloor
      || (vis == kVisMyTail && g_snakes[snake_idx].body.size() > 2)
      || vis == kVisOtherTail) {
    return true;
  }
  return false;
}

Vec2Si32 Think(Si32 snake_idx) {
  Look(snake_idx); // up?
  for (Si32 card_idx = 0; card_idx < 12; ++card_idx) {
    if (IsForwardOk(snake_idx) && TryMatch(snake_idx, card_idx)) {
      return Vec2Si32(0, 1);
    }
    RotateLook(snake_idx); // left?
    if (IsForwardOk(snake_idx) && TryMatch(snake_idx, card_idx)) {
      return Vec2Si32(-1, 0);
    }
    RotateLook(snake_idx); // down?
    if (IsForwardOk(snake_idx) && TryMatch(snake_idx, card_idx)) {
      return Vec2Si32(0, -1);
    }
    RotateLook(snake_idx); // right?
    if (IsForwardOk(snake_idx) && TryMatch(snake_idx, card_idx)) {
      return Vec2Si32(1, 0);
    }
    RotateLook(snake_idx); // up for random?
  }
  
  std::deque<Vec2Si32> dirs;
  dirs.emplace_back(0, 1);
  dirs.emplace_back(-1, 0);
  dirs.emplace_back(0, -1);
  dirs.emplace_back(1, 0);
  while (dirs.size()) {
    Si32 idx = static_cast<Si32>(g_rnd() % dirs.size());
    Vec2Si32 dir = dirs[idx];
    dirs[idx] = dirs.back();
    dirs.pop_back();
    
    Vec2Si32 head(3, 3);
    Vec2Si32 new_pos = head + dir;
    VisKind vis = g_snakes[snake_idx].vis[new_pos.y][new_pos.x].kind;
    if (vis == kVisFloor
        || (vis == kVisMyTail && g_snakes[snake_idx].body.size() > 2)
        || vis == kVisOtherTail) {
      return dir;
    }
  }
  return Vec2Si32(0, 0);
}

void Eat(Vec2Si32 pos) {
  g_eat_marks.emplace_back(pos, Time());
}

bool MoveSnake(Si32 snake_idx, Vec2Si32 dir) {
  if (dir == Vec2Si32(0, 0)) {
    return false;
  }
  Vec2Si32 new_head_pos = g_snakes[snake_idx].body.front() + dir;
  if (Map(new_head_pos).kind != kCellFloor) {
    return false;
  }
  for (Si32 idx = 0; idx < g_snakes.size(); ++idx) {
    Snake &s = g_snakes[idx];
    auto it = s.body.rbegin();
    if (it != s.body.rend()) {
      if (*it == new_head_pos) {
        
        s.body.pop_back();
        if (s.body.size()) {
          Eat(s.body.back());
        }
        g_snakes[snake_idx].body.push_front(new_head_pos);
        if (idx == snake_idx && s.body.size()) {
            s.body.pop_back();
        }
        return true;
      }
      it++;
      while (it != s.body.rend()) {
        if (*it == new_head_pos) {
          return false;
        }
        it++;
      }
    }
  }
  
  g_snakes[snake_idx].body.push_front(new_head_pos);
  g_snakes[snake_idx].body.pop_back();
  return true;
}



void RenderMap() {
  Vec2Si32 pos;
  for (pos.y = 0; pos.y < g_map_size.y; ++pos.y) {
    for (pos.x = 0; pos.x < g_map_size.x; ++pos.x) {
      Vec2Si32 scr_pos = pos * 20;
      Cell cell = Map(pos);
      Sprite &sprite = CellSprite(cell.kind);
      sprite.Draw(scr_pos);
    }
  }
  
  for (Si32 snake_idx = 0; snake_idx < g_snakes.size(); ++snake_idx) {
    Snake &s = g_snakes[snake_idx];
    auto part = s.body.begin();
    if (part != s.body.end()) {
      auto prev_part = part;
      part++;
      if (part != s.body.end()) {
        Vec2Si32 dir = *prev_part - *part;
        Sprite head = g_head_up[snake_idx];
        if (dir.y == -1) {
          head = g_head_down[snake_idx];
        } else if (dir.x == -1) {
          head = g_head_left[snake_idx];
        } else if (dir.x == 1) {
          head = g_head_right[snake_idx];
        }
        head.Draw(g_sz.x * prev_part->x, g_sz.y * prev_part->y);
      }
      while (part != s.body.end()) {
        if (part == s.body.end()) {
          break;
        }
        auto next_part = part + 1;
        if (next_part == s.body.end()) {
          break;
        }
        Vec2Si32 out_dir = *prev_part - *part;
        Vec2Si32 in_dir = *next_part - *part;
        Sprite body = g_body_ru[snake_idx];
        if (in_dir.y == -1) {
          if (out_dir.x == -1) {
            body = g_body_dl[snake_idx];
          } else if (out_dir.x == 1) {
            body = g_body_dr[snake_idx];
          } else if (out_dir.y == 1) {
            body = g_body_du[snake_idx];
          }
        } else if (in_dir.x == -1) {
          if (out_dir.y == -1) {
            body = g_body_dl[snake_idx];
          } else if (out_dir.x == 1) {
            body = g_body_lr[snake_idx];
          } else if (out_dir.y == 1) {
            body = g_body_lu[snake_idx];
          }
        } else if (in_dir.x == 1) {
          if (out_dir.y == -1) {
            body = g_body_dr[snake_idx];
          } else if (out_dir.x == -1) {
            body = g_body_lr[snake_idx];
          } else if (out_dir.y == 1) {
            body = g_body_ru[snake_idx];
          }
        } else if (in_dir.y == 1) {
          if (out_dir.y == -1) {
            body = g_body_du[snake_idx];
          } else if (out_dir.x == -1) {
            body = g_body_lu[snake_idx];
          } else if (out_dir.y == 1) {
            body = g_body_ru[snake_idx];
          }
        }
        body.Draw(g_sz.x * part->x, g_sz.y * part->y);
        
        prev_part = part;
        part++;
      }
      if (part != s.body.end()) {
        prev_part = part - 1;
        if (prev_part != s.body.end()) {
          Vec2Si32 dir = *prev_part - *part;
          Sprite tail = g_tail_up[snake_idx];
          if (dir.y == -1) {
            tail = g_tail_down[snake_idx];
          } else if (dir.x == -1) {
            tail = g_tail_left[snake_idx];
          } else if (dir.x == 1) {
            tail = g_tail_right[snake_idx];
          }
          tail.Draw(g_sz.x * part->x, g_sz.y * part->y);
        }
      }
    }
  }
  for (auto &mark : g_eat_marks) {
    g_bite.Draw(mark.pos.x * g_sz.x, mark.pos.y * g_sz.y);
  }
}

void RenderVis(Si32 snake_idx, Vec2Si32 offset) {
  Vec2Si32 pos;
  for (pos.y = 0; pos.y < g_vis_sz.y; ++pos.y) {
    for (pos.x = 0; pos.x < g_vis_sz.x; ++pos.x) {
      Sprite sprite = g_empty;
      switch (g_snakes[snake_idx].vis[pos.y][pos.x].kind) {
        case kVisFloor:
          sprite = g_empty_floor;
          break;
        case kVisOtherTail:
          sprite = g_tail_right[1];
          break;
        case kVisMyTail:
          sprite = g_tail_right[0];
          break;
        case kVisOtherBody:
          sprite = g_enemy_body;
          break;
        case kVisMyBody:
          sprite = g_own_body;
          break;
        case kVisOtherHead:
          sprite = g_head_up[1];
          break;
        case kVisObstacle:
          sprite = g_obstacle;
          break;
        case kVisMyHead:
          sprite = g_head_up[0];
          break;
        default:
          break;
      }
      sprite.Draw(g_sz.x * pos.x + offset.x, g_sz.y * pos.y + offset.y);
    }
  }
}

void RenderCard(Si32 snake_idx, Si32 card_idx, Vec2Si32 offset) {
  Vec2Si32 pos;
  for (pos.y = 0; pos.y < g_vis_sz.y; ++pos.y) {
    for (pos.x = 0; pos.x < g_vis_sz.x; ++pos.x) {
      Vec2Si32 draw_pos(g_sz.x * pos.x + offset.x, g_sz.y * pos.y + offset.y);
      
      Code &code = g_snakes[snake_idx].card[card_idx].code[pos.y][pos.x];
      Sprite sprite = g_empty;
      if (code.is_and) {
        sprite = g_and_floor;
      } else {
        sprite = g_floor;
      }
      if (code.kind == kVisNone) {
        sprite = g_none_floor;
      }
      sprite.Draw(draw_pos);
      
      sprite = g_empty;
      switch (code.kind) {
        case kVisFloor:
          sprite = g_empty_floor;
          break;
        case kVisOtherTail:
          sprite = g_tail_right[1];
          break;
        case kVisMyTail:
          sprite = g_tail_right[0];
          break;
        case kVisOtherBody:
          sprite = g_enemy_body;
          break;
        case kVisMyBody:
          sprite = g_own_body;
          break;
        case kVisOtherHead:
          sprite = g_head_up[1];
          break;
        case kVisObstacle:
          sprite = g_obstacle;
          break;
        case kVisMyHead:
          sprite = g_head_up[0];
          break;
        case kVisNone:
          sprite = g_empty;
          break;
      }
      sprite.Draw(draw_pos);
      
      if (code.is_exclude) {
        g_cross.Draw(draw_pos);
      }
    }
  }
}

void RenderTool(Vec2Si32 pos, Sprite sprite, bool is_selected) {
  if (is_selected) {
    double integral = 0.0;
    double fraction = std::modf(Time() * 2.0, &integral);
    if (fraction > 0.5) {
      return;
    }
  }
  sprite.Draw(pos);
}

void RenderTools() {
  Vec2Si32 pos = g_tools_offset;
  
  DrawText(pos.x, pos.y + g_sz.y, g_tool_info.data());
  
  Vec2Si32 step(g_sz.x, 0);
  RenderTool(pos, g_own_body, g_tool.kind == kVisMyBody);
  pos += step;
  RenderTool(pos, g_tail_right[0], g_tool.kind == kVisMyTail);
  pos += step;
  pos += step;
  RenderTool(pos, g_head_up[1], g_tool.kind == kVisOtherHead);
  pos += step;
  RenderTool(pos, g_enemy_body, g_tool.kind == kVisOtherBody);
  pos += step;
  RenderTool(pos, g_tail_right[1], g_tool.kind == kVisOtherTail);
  pos += step;
  pos += step;
  RenderTool(pos, g_empty_floor, g_tool.kind == kVisFloor);
  pos += step;
  RenderTool(pos, g_obstacle, g_tool.kind == kVisObstacle);
  pos += step;
  pos += step;
  pos += step;
  RenderTool(pos, g_cross, g_tool.is_exclude);
  pos += step;
  RenderTool(pos, g_and_floor, g_tool.is_and);
}

Vec2Si32 CardOffset(Si32 card_idx) {
  Si32 card_x = card_idx % 3;
  Si32 card_y = card_idx / 3;
  Vec2Si32 card_offset(
                       g_sz.x * (g_map_size.x + 1 + card_x * (g_vis_sz.x + 1)),
                       g_sz.y * ((3 - card_y) * (g_vis_sz.y + 1)));
  return card_offset;
}

Command InputCommand() {
  Command cmd;
  
  g_tool_info.clear();
  Vec2Si32 tools_mouse = MousePos() - g_tools_offset;
  if (tools_mouse.x >= 0 && tools_mouse.y >= 0 && tools_mouse.y < g_sz.y) {
    Si32 x_cell = tools_mouse.x / g_sz.x;
    Code new_tool = g_tool;
    switch (x_cell) {
      case 0:
        new_tool.kind = kVisMyBody;
        g_tool_info = u8"Свое тело";
        break;
      case 1:
        new_tool.kind = kVisMyTail;
        g_tool_info = u8"Свой хвостик";
        break;
      case 3:
        new_tool.kind = kVisOtherHead;
        g_tool_info = u8"Голова врага, ой-ой!";
        break;
      case 4:
        new_tool.kind = kVisOtherBody;
        g_tool_info = u8"Тело врага";
        break;
      case 5:
        new_tool.kind = kVisOtherTail;
        g_tool_info = u8"Хвостик врага, ням-ням!";
        break;
      case 7:
        new_tool.kind = kVisFloor;
        g_tool_info = u8"Пустая клетка боевой арены";
        break;
      case 8:
        new_tool.kind = kVisObstacle;
        g_tool_info = u8"Стена, снеговик или мешок с подарками";
        break;
      case 11:
        new_tool.is_exclude = !new_tool.is_exclude;
        g_tool_info = u8"Логическое отрицание, например НЕ Тело врага";
        break;
      case 12:
        new_tool.is_and = !new_tool.is_and;
        g_tool_info = u8"Логическое И, примеры: И Тело врага, И НЕ Тело врага";
        break;
    }
    if (new_tool.kind != g_tool.kind ||
        new_tool.is_and != g_tool.is_and ||
        new_tool.is_exclude != g_tool.is_exclude) {
      if (IsKeyDownward(kKeyMouseLeft)) {
        return SelectTool(new_tool);
      }
    }
  }
  
  if (IsKeyDownward(kKeyMouseLeft)) {
    for (Si32 card_idx = 0; card_idx < 12; ++card_idx) {
      Vec2Si32 card_offset = CardOffset(card_idx);
      Vec2Si32 card_pos = MousePos() - card_offset;
      if (card_pos.x >= 0 && card_pos.y >= 0) {
        Vec2Si32 pos(card_pos.x / g_sz.x, card_pos.y / g_sz.y);
        if (pos.x < g_vis_sz.x && pos.y < g_vis_sz.y) {
          if (g_tool.kind != kVisNone) {
            return EditCard(0, card_idx, pos);
          }
        }
      }
    }
  }
  return cmd;
}

void ApplyCommand(Command cmd) {
  switch (cmd.kind) {
    case kCommandEditCard:
    {
      Card &card = g_snakes[cmd.snake_idx].card[cmd.card_idx];
      Code &code = card.code[cmd.card_pos.y][cmd.card_pos.x];
      code = cmd.to_code;
    }
      break;
    case kCommandSelectTool:
      g_tool = cmd.to_code;
      break;
      
    default:
      break;
  }
}

void StepBattle() {
  if (g_battle_result != BattleInProgress) {
    return;
  }
  if (g_snakes[0].body.size() <= 1) {
    g_battle_result = BattleLost;
    return;
  }
  bool is_present = false;
  for (Si32 snake_idx = 1; snake_idx < g_snakes.size(); ++snake_idx) {
    if (g_snakes[snake_idx].body.size() >= 2) {
      is_present = true;
    }
  }
  if (!is_present) {
    g_battle_result = BattleWon;
    if (g_control != kControlEnd)
      g_buttons[g_control].state = kButtonUp;
    g_control = kControlEnd;
    return;
  }
  
  if (g_battle_step >= 1000) {
      Si32 diff = (Si32)g_snakes[0].body.size() - (Si32)g_snakes[1].body.size();
      if (diff > 3) {
          g_battle_result = BattleWon;
      } else if (diff < -3) {
          g_battle_result = BattleLost;
      } else {
          g_battle_result = BattleDraw;
      }
      if (g_control != kControlEnd)
        g_buttons[g_control].state = kButtonUp;
      g_control = kControlEnd;
      return;
  }
  
  bool is_moved = false;
  //Si32 idx = 0;
  for (Si32 idx = 0; idx < g_snakes.size(); ++idx) {
    Si32 snake_idx = static_cast<Si32>(
      (idx + g_battle_step % g_snakes.size()) % g_snakes.size());
    Vec2Si32 dir = Think(snake_idx);
    bool is_ok = MoveSnake(snake_idx, dir);
    if (is_ok) {
      is_moved = true;
    }
  }
  if (!is_moved) {
    g_battle_result = BattleDraw;
    if (g_control != kControlEnd)
      g_buttons[g_control].state = kButtonUp;
    g_control = kControlEnd;
    return;
  }
  g_battle_step++;
}


void Render() {
  Clear();
  
  RenderMap();
  char title[128];
  snprintf(title, 128, u8"Боевая арена, %lld шаг боя", (Si64)g_battle_step);
  DrawText(0, g_sz.y * g_map_size.y, title);
  
  Vec2Si32 offset(0, g_sz.y * (g_map_size.y + 1));
  RenderVis(0, offset);
  DrawText(offset.x, offset.y + g_sz.y * 7, u8"Поле зрения");
  
  for (Si32 card_idx = 0; card_idx < 12; ++card_idx) {
    Vec2Si32 pos = CardOffset(card_idx);
    RenderCard(0, card_idx, pos);
    
    char page[128];
    snprintf(page, 128, u8"Перфокарта %d", (int)(card_idx + 1));
    DrawText(pos.x, pos.y + g_sz.y * 7, page);
  }
  
  DrawText(g_tools_offset.x, g_tools_offset.y + g_sz.y * 3,
           u8"Пробел - инструкция");  
  RenderTools();
  
  
  std::string control_text;
  static std::vector<std::string> hover_text = {
    u8"Начать бой с начала",
    u8"Выполнить 1 такт боя и остановить время",
    u8"Выполнять бой на нормальной скорости",
    u8"Выполнять бой в ускоренном темпе",
    u8"Быстро выполнить 100 боев",
    u8""
  };
  for (Si32 control = kControlBegin; control < kControlEnd; ++control) {
    g_buttons[control].Draw();
    if (g_buttons[control].state == kButtonHover) {
      
      control_text = hover_text[control];
    }
  }
  DrawText(g_sz.x * (g_vis_sz.x + 1),
           g_sz.y * (g_map_size.y + 1) + 40,
           control_text.data());
  
  
  DrawText(g_sz.x * (g_vis_sz.x + 1) + 40 * 5 + 10,
           g_sz.y * (g_map_size.y ),
           g_100_stats.data());
  
  
  char level[128];
  snprintf(level, 128, u8"Уровень %d", (int)(g_level_idx));
  DrawText(g_sz.x * (g_vis_sz.x + 1), g_sz.y * (g_map_size.y + 8), level);
  
  DrawText(g_sz.x * (g_vis_sz.x + 1), g_sz.y * (g_map_size.y + 6),
           u8"Для перехода на следующий уровень необходимо");
  DrawText(g_sz.x * (g_vis_sz.x + 1), g_sz.y * (g_map_size.y + 6) - 11,
           u8"выиграть из 100 боев на 10 больше, чем про-");
  DrawText(g_sz.x * (g_vis_sz.x + 1), g_sz.y * (g_map_size.y + 6) - 11 * 2,
           u8"играть.");

  
  
  Sprite battle_state = g_empty;
  switch (g_battle_result) {
    case BattleInProgress:
      break;
    case BattleLost:
      battle_state = g_wasted;
      break;
    case BattleWon:
      battle_state = g_glory;
      break;
    case BattleDraw:
      battle_state = g_draw;
      break;
    case BattleLevelComplete:
      battle_state = g_complete;
      break;
  }
  battle_state.Draw((g_sz.x * g_map_size.x - battle_state.Width()) / 2,
                    g_sz.y * g_map_size.y / 2);
  
  ShowFrame();
}

void SaveAi(Si32 snake_idx, const char *file_name) {
  std::vector<Ui8> data;
  Snake &s = g_snakes[snake_idx];
  for (Si32 card_idx = 0; card_idx < 12; ++card_idx) {
    Card &card = s.card[card_idx];
    for (Si32 y = 0; y < g_vis_sz.y; ++y) {
      for (Si32 x = 0; x < g_vis_sz.x; ++x) {
        Code &code = card.code[y][x];
        data.push_back(code.kind);
        data.push_back(code.is_and);
        data.push_back(code.is_exclude);
        data.push_back(0);
      }
    }
  }
  WriteFile(file_name, (const Ui8*)data.data(), data.size());
}

bool LoadAi(Si32 snake_idx, const char *file_name) {
    std::ifstream in(file_name, std::ios_base::in | std::ios_base::binary);
    if (in.rdstate() == std::ios_base::failbit) {
        return false;
    }
    in.close();

  std::vector<Ui8> data = ReadFile(file_name);
  Si32 idx = 0;
  Snake &s = g_snakes[snake_idx];
  for (Si32 card_idx = 0; card_idx < 12; ++card_idx) {
    Card &card = s.card[card_idx];
    for (Si32 y = 0; y < g_vis_sz.y; ++y) {
      for (Si32 x = 0; x < g_vis_sz.x; ++x) {
        Code &code = card.code[y][x];
        code.kind = static_cast<VisKind>(data[idx]);
        ++idx;
        code.is_and = (data[idx] != 0);
        ++idx;
        code.is_exclude = (data[idx] != 0);
        ++idx;
        ++idx;
      }
    }
  }
  return true;
}

void LoadAi() {
    char filename[128];
    snprintf(filename, 128, "data/%d.snake", g_level_idx);
    if (LoadAi(1, filename)) {
        return;
    }
    SaveAi(0, "data/ultimate.snake");
    LoadAi(1, "data/ultimate.snake");
}


void Update() {
  double time = Time();
  double dt = time - g_prev_time;
  g_prev_time = time;
  while (!g_eat_marks.empty() && g_eat_marks.front().time + 0.125f < Time()) {
    g_eat_marks.pop_front();
  }
  
/*  //Update keyboard
  if (IsKeyDownward("S")) {
    SaveAi(0, "data/my.snake");
  }
  if (IsKeyDownward("L")) {
    LoadAi(0, "data/my.snake");
  }
  if (IsKeyDownward("E")) {
    LoadAi(1, "data/my.snake");
  }
  */
  
  // Update editor
  Command cmd = InputCommand();
  if (cmd.kind != kCommandNone && cmd.from_code.kind != kVisMyHead) {
    ApplyCommand(cmd);
  }
  
  //UpdateControls
  switch (g_control) {
    case kControlRestart:
      SaveAi(0, "data/save.game");
      ResetBattle();
      if (g_control != kControlEnd)
        g_buttons[g_control].state = kButtonUp;
      g_control = kControlEnd;
      break;
    case kControlPlay:
      SaveAi(0, "data/save.game");
      if (g_battle_result != BattleInProgress) {
          ResetBattle();
      }
      if (time - g_prev_step_time > 0.125) {
        g_prev_step_time = time;
        StepBattle();
      }
      break;
    case kControlStep:
        if (g_battle_result != BattleInProgress) {
            ResetBattle();
        }
      SaveAi(0, "data/save.game");
      StepBattle();
      if (g_control != kControlEnd)
        g_buttons[g_control].state = kButtonUp;
      g_control = kControlEnd;
      break;
    case kControlPlayFast:
        if (g_battle_result != BattleInProgress) {
            ResetBattle();
        }
      SaveAi(0, "data/save.game");
      if (time - g_prev_step_time > 0.016) {
        g_prev_step_time = time;
        StepBattle();
        StepBattle();
      }
      //g_buttons[g_control].state = kButtonUp;
      //g_control = kControlEnd;
      break;
    case kControlPlay100: {
        SaveAi(0, "data/save.game");

        Si32 won = 0;
      Si32 lost = 0;
      for (Si32 i = 0; i < 100; ++i) {
        ResetBattle();
        while (g_battle_result == BattleInProgress) {
          StepBattle();
        }
        if (g_battle_result == BattleWon) {
          won++;
        }
        if (g_battle_result == BattleLost) {
          lost++;
        }
        g_100_stats.resize(128);
        snprintf(const_cast<char*>(g_100_stats.data()), 128,
                u8"%d побед %d поражений", won, lost);
        Render();
        ShowFrame();
      }
      if (g_control != kControlEnd)
        g_buttons[g_control].state = kButtonUp;
      g_control = kControlEnd;
      if (won - lost >= 10) {
        g_level_idx++;
        g_battle_result = BattleLevelComplete;
        LoadAi();
      }
      
      break;
    }
    case kControlEnd:
      break;
  }
  for (Si32 control = kControlBegin; control < kControlEnd; ++control) {
    g_buttons[control].Update();
  }
  Control other_control = kControlEnd;
  for (Si32 control = kControlBegin; control < kControlEnd; ++control) {
    if (control != g_control) {
      if (g_buttons[control].state == kButtonDown) {
        other_control = static_cast<Control>(control);
      }
    }
  }
  if (other_control != kControlEnd) {
    if (g_control != kControlEnd) {
      g_buttons[g_control].state = kButtonUp;
    }
    g_control = other_control;
  }
  
  
  
  for (Si32 snake_idx = 0; snake_idx < g_snakes.size(); ++snake_idx) {
    Look(snake_idx);
  }
  
}

bool CheckKeys() {
  if (IsKeyDownward(kKeyEscape) || IsKeyDownward(kKeySpace) || IsKeyDownward(kKeyEnter)) {
    return true;
  }
  if (IsKeyDownward("qwertyuiop[]\asdfghjkl;'zxcvbnm,./ 1234567890-=")) {
      return true;
  }
  return false;
}

void ShowHelp() {
    while (!IsKeyDown(kKeyEscape)) {
        Clear();
        g_help.Draw(0, 0);
        ShowFrame();
        
        if (CheckKeys()) {
            return;
        }
    }
}

void ShowDebug() {
  return;
    while (!IsKeyDown(kKeyEscape)) {
        Clear();
        char text[128];
        snprintf(text, 128, "window size %d x %d", arctic::WindowSize().x, WindowSize().y);
        DrawText(0, 0, text);
        ShowFrame();
    }
}

void EasyMain() {
  Init();
  ShowDebug();
  LoadAi();
  LoadAi(0, "data/save.game");

  ShowHelp();
  while (!IsKeyDown(kKeyEscape)) {
    Update();
    Render();

    if (CheckKeys()) {
        ShowHelp();
    }
  }
}
