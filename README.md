# README #

Игра на Arctic Engine на конкурс игр на одном экране на gamedev.ru
Обсуждение проекта: https://arcticengine.com/#!/projects:aimelee
 
Конкурсная тема проекта на gamedev.ru: https://gamedev.ru/projects/forum/?id=232487

Чтобы собрать и запустить ai_melee нужно забрать репозиторий Arctic Engine в папку на том же уровне что и папка репозитория ai_melee.
Например, у меня на рабочем столе рядом забраны папки arctic и ai_melee и все собирается.